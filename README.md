![alt text](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/-/raw/master/icons/logo_long.svg)



# Vision Pipeline

[VPFR](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros)

[Wiki](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/wikis/home)

---

# mul (Test algorithm)
This is an algorithm that multiplies a number by another.  
This is used here to test the pipeline.  
[Math Test VPFR](https://gitlab.com/vision-pipeline-for-ros/types/math-test-vpfr) is responsible for this as the visionpipeline type.  


Details on this in the [wiki](https://gitlab.com/vision-pipeline-for-ros/algorithms/mul/-/wikis/home)
