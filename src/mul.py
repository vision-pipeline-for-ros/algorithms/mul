"""
math-test-vpfr
Copyright 2020 Vision Pipeline for ROS
Author: Fabian Kranewitter <fabiankranewitter@gmail.com>
Author: Sebastian Bergner <sebastianbergner5892@gmail.com>
SPDX-License-Identifier: MIT
"""
from std_msgs.msg import Int64

from algorithm_template import AlgorithmTemplate


class Mul(AlgorithmTemplate):
    def main(self, req):
        return req.data * self.__number

    def on_config_change(self, configdata):
        self.__number = configdata.get("number")[0]

    def get_io_type(self):
        return Int64, Int64

    def on_enable(self):
        pass

    def on_disable(self):
        pass

    def on_additional_data_change(self, data):
        pass
